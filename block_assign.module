<?php

/**
 * @file
 */

/**
 * Implementation of hook_help().
 */
/**
 * Display help and module information
 * @param section which section of the site we're displaying help
 * @return help text for section
 */
function block_assign_help($section='') {

  $output = '';

  switch ($section = '') {
    case "admin/help#block_assign":
      $output = '<p>'. t("") .'</p>';
      break;
    case "admin/modules/#description":
      $output = '<p>'. t('block_assign help') .'</p>';
      break;
  }

  return $output;
}

function block_assign_block_admin_submit($form_id, $form_values) {
    if (function_exists('block_assign_show')) {
      $sql = 'UPDATE blocks SET visibility = 2, pages = CONCAT("<", "?", "php ", "if (function_exists(\'block_assign_show\')) { return block_assign_show(\'", module, "\',\'", delta, "\'); }", "?", ">") WHERE status=1 AND theme=\'%s\'';
      db_query($sql, variable_get('theme_default', 'garland'));
  }
}

function block_assign_form_alter($form_id, &$form) {
  if ($form_id == 'block_admin_display') {
    $form['#submit']['block_assign_block_admin_submit'] = array();
  }
  elseif ($form_id == 'block_admin_configure') {
    $module = arg(4);
    $delta = arg(5);
    $form['page_vis_settings']['visibility']['#disabled'] = TRUE;
    $form['page_vis_settings']['pages']['#disabled'] = TRUE;
    $form['page_vis_settings']['#description'] = t('Page specific visibility is being handled by your <strong>block_assign_show()</strong> function.');
    $form['#submit']['block_assign_block_admin_submit'] = array();
  }
}


function block_assign_menu($may_cache) {
  $items[] = array(
    'path' => 'admin/settings/block_assign',
    'title' => t('Block Assign Settings'),
    'callback' => 'block_assign_admin',
    /*'callback' => 'drupal_get_form',
    'callback arguments' => 'block_assign_admin',*/
    'access' => user_access('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  $arg = intval(arg(3));
  if ($arg > 0) {
    $items[] = array(
      'path' => 'admin/settings/block_assign/'. $arg,
      'title' => t('Edit Block Assignment'),
      'callback' => 'block_assign_admin',
      'access' => user_access('access administration pages')
    );
  }

  $items[] = array(
    'path' => 'admin/build/block/visibility',
    'title' => t('Page Visibility'),
    'callback' => 'block_assign_admin',
    'access' => user_access('access administration pages'),
    'type' => MENU_LOCAL_TASK,
  ); 
  return $items;
}

function block_assign_edit_form() {
  $id = intval(arg(3));
  $form = array();

  if ($id > 0) {
    $block = db_fetch_object(db_query('SELECT * FROM {block_assign} WHERE id = %d', $id));
    $blocks = module_invoke($block->module, 'block');
    $title = array_key_exists($block->box_id, $blocks) ? $blocks[$block->box_id]['info'] : t('Unknown');

    $types = array('node' => t('Node ID'), 'uri' => t('URI Regex'));
    if (function_exists('globalnode_nodeapi')) {
      $types['type'] = t('Node Type');
    }

    $form['#base'] = 'block_assign_edit_form';
    $form['block_assign_id'] = array(
      '#type'  => 'hidden',
      '#value'  => $id
    );
    $form['block_assign_title'] = array(
      '#id'      => 'block_assign_title',
      '#type'  => 'item',
      '#value'  => '<strong>'. t('Title') .': </strong>'. $title
    );
    $form['block_assign_type'] = array(
      '#id'      => 'block_assign_type',
      '#type'  => 'select',
      '#options'      => $types,
      '#value'  => $block->condition_type
    );
    $form['block_assign_value'] = array(
      '#id'        => 'block_assign_value',
      '#type'    => 'textfield',
      '#default_value'  => stripslashes($block->condition_value)
    );
    $form['submit'] = array(
      '#id'      => 'submit',
      '#type'  => 'submit',
      '#value'  => t('Save')
    );
    $form['delete'] = array(
      '#id'      => 'delete',
      '#type'  => 'submit',
      '#value'  => t('Delete'),
      '#attributes'   => array('onclick' => 'return confirm("Are you sure you want to delete?")')
    );
  }
  return $form;
}

function block_assign_new_form() {
  $form = array();
  $form['#base'] = 'block_assign_new_form';
  $types = array('node' => t('Node ID'), 'uri' => t('URI Regex'));
  if (function_exists('globalnode_nodeapi')) {
    $types['type'] = t('Node Type');
  }

  $form['new'] = array(
    '#type'  => 'fieldset',
    '#title'  => t('Add New Block Association'),
  );
  $form['new']['top'] = array(
    '#type'   => 'item',
    '#value'  => ' ',
    '#prefix'       => '<table>',
    '#weight'       => -100
  );

  $form['new']['instructions'] = array(
    '#type'   => 'item',
    '#weight'       => -90,
    '#value'  => t('First, select the block(s) you would like to add. Then add values in the fields below for any search criteria you wish to set. Leave blank to set nothing.')
  );

  $instructions = array(
    'type'    => t('Enter a node type (e.g. "blog") if you wish to create a block association for the selected blocks.'),
    'node'    => t('Enter a node id (e.g. "3") if you wish to create a block association for that node.'),
    'uri'     => t('Enter a regular expression (e.g. "^\/blog") to be searched against the request uri. Omit starting and ending slashes and be sure to escape characters that typically must be escaped.')
  );

  //SELECT bid, info FROM {boxes} ORDER BY info
  $blocks = array();
  $result = db_query('SELECT module, delta FROM {blocks} WHERE status = 1');
  while ($row = db_fetch_object($result)) {
    $bl = module_invoke($row->module, 'block');
    $blocks[$row->module .':'. $row->delta] = $bl[$row->delta]['info'];
  }
  asort($blocks);
  $form['new']['block_assign_blocks'] = array(
    '#id'      => 'block_assign_blocks',
    '#type'  => 'select',
    '#title'  => t('Block'),
    '#options'      => $blocks,
    '#weight'       => -8,
    '#multiple'     => TRUE,
    '#prefix'       => '<tr><td colspan="2">',
    '#suffix'       => '</td></tr>'
  );

  foreach ($types as $k => $v) {
    $form['new']['block_assign_type_'. $k] = array(
      '#type'   => 'item',
      '#value'  => '<strong>'. $v .'</strong>',
      '#prefix'       => '<tr><td valign="top">',
      '#suffix'       => '</td>',
      //'#weight'     => $weight
    );
    $form['new']['block_assign_value_'. $k] = array(
      '#id'     => 'block_assign_value_'. $k,
      '#type'   => 'textfield',
      '#title'  => t('Value'),
      //'#weight'       => $weight,
      //'#description'  => t('Enter the value the module should search for. If "Node ID" is selected above, this is an integer. If "URI Regex" is selected, this is a regular expression (minus slashes). If "Node Type" is available and selected, this should be a node type.'),
      '#description'  => $instructions[$k],
      '#prefix'       => '<td valign="top">',
      '#suffix'       => '</td></tr>'
    );
  }

  $form['new']['block_add_new'] = array(
    '#id'      => 'block_add_new',
    '#type'  => 'submit',
    '#value'  => t('Add New'),
    '#prefix'       => '<tr><td colspan="2">',
    '#suffix'       => '</td></tr>'
  );

  $form['new']['bottom'] = array(
    '#type'   => 'item',
    '#value'  => ' ',
    '#suffix'       => '</table>',
    '#weight'       => 100
  );

  return $form;
}

function block_assign_admin() {
  if ($_POST) {
    switch ($_POST['op']) {
      case 'Add New':
        $types = array('type', 'node', 'uri');
        if (sizeof($_POST['block_assign_blocks']) > 0) {
          foreach ($_POST['block_assign_blocks'] as $block) {
            list($module, $delta) = split(":", $block);
            foreach ($types as $type) {
              if ($_POST['block_assign_value_'. $type] != '') {
                db_query('INSERT INTO {block_assign} (module, condition_type, condition_value, box_id) VALUES ("%s", "%s", "%s", "%s")', $module, $type, $_POST['block_assign_value_'. $type], $delta);
              }
            }
          }
          drupal_set_message(t('New Block Assign associations added.'));
        }
        else{
          drupal_set_message(t('No blocks were selected.'));
        }
        break;
      case 'Save':
        db_query('UPDATE {block_assign} SET condition_type = "%s", condition_value = "%s" WHERE id = %d', $_POST['block_assign_type'], $_POST['block_assign_value'], intval($_POST['block_assign_id']));
        drupal_set_message(t('Block edits completed.'));
        break;
      case 'Delete':
        db_query('DELETE FROM {block_assign} WHERE id = %d', intval($_POST['block_assign_id']));
        drupal_set_message(t('Block assignment deleted.'));
        break;
    }
    drupal_goto('admin/settings/block_assign');
  }
  else{
    if (intval(arg(3)) > 0) {
      $output = drupal_get_form('block_assign_edit_form');
    }
    else{
      $output = drupal_get_form('block_assign_new_form');

      $result = db_query('SELECT ba.id, b.module, b.delta, ba.condition_type type, ba.condition_value value, MD5(CONCAT(ba.condition_type, ba.condition_value)) AS sep FROM {blocks} b, {block_assign} ba WHERE b.status = 1 AND b.module = ba.module AND b.delta = ba.box_id ORDER BY type, value');

      $data = array();
      $blocks = array();
      $titles = array();
      $sep = '';
      while ($row = db_fetch_object($result)) {
        $blocks[] = $row;
        $info = module_invoke($row->module, 'block');
        foreach ($info as $k => $v) {
          $titles[$row->module .'_'. $k] = $v['info'];
        }
      }

      foreach ($blocks as $row) {
        if ($sep != $row->sep) {
          $data[] = array('<strong>Type '. $row->type .' for value '. $row->value .'</strong>', '', '', '');
          $sep = $row->sep;
        }
        $data[] = array(
          $titles[$row->module .'_'. $row->delta],
          $row->type,
          $row->value,
          '<a href="/admin/settings/block_assign/'. intval($row->id) .'">'. t('edit') .'</a>',
        );
      }
      $headers = array(t('Block Name'), t('Type'), t('Value'), t('Edit'));
      $output .= theme('table', $headers, $data);
    }
    return $output;
  }
}

function block_assign_show($module, $delta) {
  $conditions = array();
  if (!array_key_exists('block_assign_onditions', $_SESSION)) {
    $result = db_query('select b.module, ba.condition_type, ba.condition_value, b.delta AS bid from {block_assign} ba, {blocks} b WHERE ba.module = b.module AND ba.box_id = b.delta AND ba.module = "%s" AND ba.box_id = "%s"', $module, $delta);
    while ($item = db_fetch_object($result)) {
      $conditions[] = $item;
    }
    $_SESSION['block_assign_conditions'] = $conditions;
  }
  else{
    $conditions = $_SESSION['block_assign_conditions'];
  }


  foreach ($conditions as $condition) {
    switch ($condition->condition_type) {
      case 'node':
        preg_match('/^node\/(\d*)$/', $_GET['q'], $matches);
        if ($matches[1] && $condition->condition_value == $matches[1]) {
          error_log('node matches '. $matches[1]);
          print('<pre>'. print_r($types, 1) .'</pre>');
          return true;
        }
        break;
      case 'type':
        if (isset($GLOBALS['globalnode']) && isset($GLOBALS['globalnode']->type) && $GLOBALS['globalnode']->type == $condition->condition_value) {
          error_log('type matches '. $condition->condition_value);
          print('<pre>'. print_r($GLOBALS['globalnode'], 1) .'</pre>');
          return true;
        }
        break;
      case 'uri':
        if (preg_match('/'. $condition->condition_value .'/', request_uri())) {
          return true;
        }
        break;
    }
  }

  return false;
}

